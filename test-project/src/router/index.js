import Vue from 'vue'
import Router from 'vue-router'
import Basket from '@/components/Basket.vue'
import CatalogList from '@/components/CatalogList.vue'

Vue.use(Router)

export default new Router ({
    routes: [
        {
            path: '/basket',
            name: 'basket',
            component: Basket

        },

        {
            path: '/',
            name: 'catalogList',
            component: CatalogList

        }
    ]
})