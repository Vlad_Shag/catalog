export default {
    state: {
        goods: JSON.parse (localStorage.getItem ("elem")),
        basketGoods: JSON.parse (localStorage.getItem ("basketElem")),
        goodsId: null,
        amount: {price: 0, arrID: []},
    },

    actions: {
        
    },

    mutations: {
        updateGoods (state, product) {
            let goods;
            
            if (product.addTo != 'like') {
                goods = state.basketGoods;
            }

            else {
                goods = state.goods;
                product.product.count = 1;
            }
            if (goods == null) {
                goods = [{
                    id: product.product.id, 
                    name: product.product.name, 
                    price: product.product.price,
                    src: product.product.src,
                    date: product.product.date,
                    count: product.product.count
                }];
            }

            else {
                goods.push ({
                    id: product.product.id, 
                    name: product.product.name, 
                    price: product.product.price,
                    src: product.product.src,
                    date: product.product.date,
                    count: product.product.count
                });   
            }

            if (product.addTo == 'like') {
                localStorage.setItem ("elem", JSON.stringify(goods));
                state.goods = goods;
            }

            else {
                localStorage.setItem ("basketElem", JSON.stringify(goods));
                state.basketGoods = goods;
            }
        },

        deleteGoods(state, product) {
            state.goodsId = product.id;
            let goods;
            if (product.delete != 'like') {
                goods = state.basketGoods;
            }
            else {
                goods = state.goods;
            }
            for(let j = 0; j < goods.length; j++) {
                if (product.id == goods[j].id) {
                    goods.splice(j, 1);
                    if (product.delete == 'like') {
                        localStorage.setItem ("elem", JSON.stringify(goods));
                        state.goods = goods;
                    }
                    else {
                        localStorage.setItem ("basketElem", JSON.stringify(goods));
                        state.basketGoods = goods;
                    }
                }  
            }
        },

        changeCount(state, product) {
            if (state.basketGoods != null) {
                for(let j = 0; j < state.basketGoods.length; j++) {
                    if (product.id == state.basketGoods[j].id) {
                        state.basketGoods[j].count = product.count;
                    }
                }
                localStorage.setItem ("basketElem", JSON.stringify(state.basketGoods));
            }
        },

        calculate(state, element) {
            if (state.amount.arrID.find(value => value == element.id) === undefined) {
                state.amount.arrID.push(element.id);
            }

            if(element.checked) {
                state.amount.price = 0; 
            }

            for(let i = 0; i < state.amount.arrID.length; i++) {
                for(let j = 0; j < state.basketGoods.length; j++) {
                    if (element.checked && state.amount.arrID[i] == state.basketGoods[j].id) {
                        state.amount.price += state.basketGoods[j].count * state.basketGoods[j].price;
                    }
                    else if (state.amount.arrID[i] == state.basketGoods[j].id && state.amount.arrID[i] == element.id) {
                        state.amount.price -= state.basketGoods[j].count * state.basketGoods[j].price;  
                        state.amount.arrID.splice(i, 1);
                        if (state.amount.price < 0) {
                            state.amount.price = 0;
                        }          
                    }
                }
            }
        }
    },

    getters: {
        getGoods(state) {
            return state.goods;
        },

        getGoodsId(state) {
            return state.goodsId;
        },

        getBasketGoods(state) {
            return state.basketGoods;            
        },

        getAmount(state) {
            return state.amount;
        }
    }
}